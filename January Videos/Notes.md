# Differential Equations Everywhere

## Position, Velocity, Acceleration

___Defn___: A differential equation (DE) is an equation involving one or more derivatives of an unknown
function.

e.g.

$y''(t) = g$ where $g = \bigg\{\begin{matrix} 9.91 \text{ m/sec}^2 \\ 32 \text{ ft/sec}^2 \end{matrix}​$

The unknown function here is $y(t)$. The goal is to solve for that unknown function.

Let $y(t)$ be the position of the object at time $t$,
then $y'(t)$ is the velocity of the object at time $t$, 
and $y''(t)$ is the acceleration of the object at time $t$.

For an object moving through space subject only to gravitational force (object is in
free fall), then its position is governed by the DE $y''(t) = g$.

$$
\begin{aligned}
& y''(t) = g \\
\implies &y'(t) = \int g \ dt = gt + C \\
\implies &y(t) = \int gt \ dt = \frac12 gt^2 +Ct + D
\end{aligned}
$$

$y(t) = \frac12 gt^2 +Ct + D$ is the **general solution** to the DE.

___Note___: if we are given **initial values** ($y(0)$, $y'(0)$, ...), then we can solve
for the constants ($C$, $D$, ...) in the general solution to obtain a **particular solution**.

___Defn___: A DE with initial conditions/values is an **initial value problem**.

---

___Example___: A boy 2m tall shoot a rocket straight up at 10 m/sec. What is the highest point reached
by the rocket?

___Solution___:

Our DE is $y''(t) = g$, where $g = 9.81$, meaning that downward is positive.

The general solution is $y(t) = \frac12 gt^2 + Ct + D$.

Initial values:

- $y'(0) = -10$ (negative because downward is positive)
- $y(0) = 0$ (true if $y$-axis starts where the object starts its trajectory)

Note:

$$
\begin{aligned}
y(0) &= D = 0 \\
y'(0) &= C = -10
\end{aligned}
$$

The particular solution is:

$$
\begin{aligned}
y(t) = \frac12 gt^2 - 10t
\end{aligned}
$$

Set $y'(t) = 0$, then $t = \frac{10}g$, thus

$$
\begin{aligned}
y\left(\frac{10}9\right) &= \frac12 g \left(\frac{10}g\right)^2 - 10 \left(\frac{10}g\right) \\
&= -\frac{50}g
\end{aligned}
$$

Height above the ground: $\frac{50}g + 2$ meters.

___Follow-up___: When does it hit the ground?

Set $y(t) = 2$, then

$$
\begin{aligned}
\frac12 gt^2 - 10t &= 2 \\
\frac12 gt^2 - 10t - 2 &= 0 \\
\end{aligned}
$$

$$
\begin{aligned}
t &= \frac{10 \plusmn \sqrt{100 + 4g}}{g} \\
\end{aligned}
$$

Take the positive value, which is $\frac{10 + \sqrt{100 + 4g}}{g}​$.

---

## Newton's Law of Cooling

> The rate of change of the temperature of an object is proportional to the temperature
> difference between the object and the surrounding medium.

Let $T(t)$ be the temperature of the object at time $t$.

Let $T_m$ be the temperature of the medium. Assume it's a constant for simplicity.

The rate of change $\frac{dT}{dt}$ is proportional to the constant $k$:

$$
\begin{aligned}
\frac{dT}{dt} = -k \cdot (T - T_m), \ \ \ k > 0
\end{aligned}
$$

---

## Mixing Problems

___Example___: There's a tank containing $A(t)$ amount of chemical at time $t$ with a
faucet pouring more chemicals in and a leak draining chemical out at the same time.

Notations:

- $r_1$: inflow (pouring faucet) rate in liters/minute.
- $r_2$: outflow (leak) rate in liters/minute
- $C_1$: concentration of chemical into the tank (grams/liters).
- $C_2$: concentration of chemical out of the tank (grams/liters).

$$
\begin{aligned}
C_2 = \frac{A(t)}{V(t)} = \frac{A(t)}{V_0 + (r_1 - r_2) t}
\end{aligned}
$$

$\frac{dA}{dt}$ is the "rate of chemical" in - "rate of chemical" out in grams/minute.

$$
\begin{aligned}
&\frac{dA}{dt} = r_1 C_1 - r_2 C_ 2 \\
&\frac{dA}{dt} + r_2 C_2 = r_1 C_1 \\
&\frac{dA}{dt} + \frac{r_2}{V} \cdot A = r_1 C_1
\end{aligned}
$$

which is the equation for solving the amount of chemical in the tank at time $t$.

---

# Differential Equation: The Basics

___Defn___: A DE has **$n$th-order** if it can be written in the form in the form $G(x, y, y', y'', ..., y^{(n)}) = 0$.

___Defn___: A DE is linear if it has the form $a_0(x)y^{(n)} + a_1(x)y^{(n-1)} + ... + a_{n-1} (x) y' + a_n (x) y = F(x)$.

___Example___:

$x^2 y'''' + \frac3x y'' = \sin x$ is 4th-order linear.

___Example___:

$y'' + 3x (y')^3 - y = 1 + 3x$ is 2nd-order non-linear.

___Example___:

$e^x sin(y') + 2xy = 5e^x$ is 1st-order non-linear.

___Example___:

$\frac{dT}{dt} = -k (T-T_m) \implies T' + kT = kT_m$ is 1st-order linear.

---

___Example___: Solve $x^2 y'' + 3xy' - 8y = 0$ by guessing $y(x) = x^r$.

___Solution___:

$$
\begin{aligned}
y' &= rx^{r-1} \\
y'' &= r(r-1) x^{r-2}
\end{aligned}
$$

Plug into the DE:

$$
\begin{aligned}
x^2 r(r-1) x^{r-2} + 3x \cdot rx^{r-1} - 8x^r &= 0 \\
r(r-1) x^r + 3rx^r - 8x^r &= 0 \\
\left[ r(r-1) + 3r - 8 \right] x^r &= 0, \ x^r \not= 0 \\
\end{aligned}
$$

$\implies r = -4$ or $r = 2$

$\implies y_1(x) = x^{-4}$ or $y_2(x) = x^2$

---

___Example___: Solve $y'' + 3y' - 10y = 0$ by guessing $y(x) = e^{rx}$ and solve for $r$.

___Solution___:

$$
\begin{aligned}
y' &= re^{rx} \\
y'' &= r^2 e^{rx}
\end{aligned}
$$

Plug into the DE:

$$
\begin{aligned}
r^2 e^{rx} + 3re^{rx} - 10 e^{rx} &= 0 \\
(r^3 + 3r - 10) e^{rx} &= 0, \ e^{rx} \not= 0 \\
\end{aligned}
$$

$\implies (r+5)(r-2) = 0$

$\implies r = -5$ or $r = 2$

$\implies y_1(x) = e^{-5x}$ or $y_2(x) = e^{2x}​$

---

# Separable Differential Equations

___Defn___: A 1st-order DE is separable if it can be written in the form $p(y) \dydx = q(x)$ or $p(y) \ dy = q(x) \ dx$.

To solve this DE, we integrate both sides and, if possible, solve for $y$ explicitly.

---

___Example___: Solve $y' = \frac{y^2}{x^2 + 1}$.

___Solution___:

$$
\begin{aligned}
y' &= \frac{y^2}{x^2 + 1} \\
\frac{dy}{dx} &= \frac{y^2}{x^2 + 1} \\
\frac{1}{y^2} \ dy &= \frac{1}{x^2 + 1} \ dx \\
\end{aligned}
$$

$$
\begin{aligned}
&\implies &\int \frac{1}{y^2} \ dy &= \int \frac{1}{x^2 + 1} \ dx \\
&\implies &-\frac1y &= \tan^{-1} (x) + C \\
&\implies &y &= -\frac{1}{tan^{-1}(x) + C}
\end{aligned}
$$

---

___Example___: Solve $(1-x^2) y' + xy = 4x$ with $y(0) = 8$.

___Solution___:

$$
\begin{aligned}
(1-x^2) \dydx &= x(4-y) \\
\frac{1}{4-y} \ dy &= \frac{x}{1-x^2} \ dx \\
\end{aligned}
$$

$$
\begin{aligned}
\implies \int \frac{1}{4-y} \ dy &= \int \frac{x}{1-x^2} \ dx \\
\int \frac{1}{y-4} \ dy &= \int \frac{x}{x^2-1} \ dx
\end{aligned}
$$

Let $u = y-4 \implies \ln|y-4|$

Let $u = x^2 - 1 \implies \frac12 \ln |x^2 - 1| + C$

$$
\begin{aligned}
\int \frac{1}{y-4} \ dy &= \int \frac{x}{x^2-1} \ dx \\
\implies \ln |y-4| &= \frac12 \ln |x^2 - 1| + C \\
|y-4| &= e^{\frac12 \ln |x^2 - 1| + C} \\
&= e^C \cdot e^{\frac12 \ln |x^2 - 1|} \\
&= C_1 \cdot e^{\ln \big( | x^2 - 1 |^{\frac{1}{2}} \big)} \\
&= C_1 \cdot \sqrt{|x^2 - 1|} \\
\end{aligned}
$$

$$
\begin{aligned}
\implies y-4 &= \plusmn C_1 \cdot \sqrt{|x^2 - 1|} \\
y-4 &= C_2 \cdot \sqrt{|x^2 - 1|} \\
\end{aligned}
$$

$$
\begin{aligned}
\implies y &= 4 + C_2 \cdot \sqrt{|x^2 - 1|}
\end{aligned}
$$

Use initial condition $y(0) = 8$:

$$
\begin{aligned}
8 &= 4 + C_2 \sqrt{|0^2 - 1|} \\
&= 4 + C_2 \\
\implies C_2 &= 4
\end{aligned}
$$

Particular solution:

$$
\begin{aligned}
\implies y &= 4 + 4 \sqrt{|x^2 - 1|}
\end{aligned}
$$

---

___Example___: Solve:

$$
\begin{aligned}
\frac{dT}{dt} = -k (T - T_m)
\end{aligned}
$$

___Solution___:

$$
\begin{aligned}
\frac{1}{T-T_m} \ dT &= -k \ dt \\
\implies \int \frac{1}{T-T_m} \ dT &= \int -k \ dt \\[1em]
\ln |T-T_m| &= -kt + C \\[.75em]
e^{\ln |T-T_m|} &= e^{-kt + C} \\
&= C_1 \cdot e^{-kt} \\[.75em]
\implies T-T_m &= \plusmn C_1 \cdot e^{-kt} \\
&= C_2 \cdot e^{-kt} \\
\implies T(t) &= T_m + C_2 \cdot e^{-kt} \ \ (k > 0)
\end{aligned}
$$

---

# Linear Differential Equation, 1st order

Form: $a(x)y' + b(x)y = F(x)$

divided by $a(x)$: $y' + p(x)y = q(x)$

Note that this equation is not separable.

Multiply the equation above by an integrating factor $I(x)$ to be determined:

$$
\begin{aligned}
I \cdot y' + p(x) \cdot Iy = g(x) \cdot I
\end{aligned}
$$

For the left side t obe a product rule, we need $\frac{dI}{dx} = p(x)\cdot I$, which is separable:

$$
\begin{aligned}
\frac{dI}{dx} &= p(x)\cdot I \\
\implies \frac{dI}{I} &= p(x) \ dx \\
\implies \int \frac{dI}I &= \int p(x) \ dx \\
\ln |I| &= \int p(x) \ dx \\
|I| &= e^{\int p(x) \ dx} \\
I &= e^{\int p(x) \ dx}
\end{aligned}
$$

We took the simplest solution, which is the positive $I(x)$ without the $C$ constant:

$$
\begin{aligned}
I(x) &= e^{\int p(x) \ dx}
\end{aligned}
$$

Then, our DE becomes:

$$
\begin{aligned}
[I \cdot y]' = g(x) \cdot I
\end{aligned}
$$

To solve for $y$, integrate both sides and divide by $I$ to isolate $y$.

---

___Example___: Solve $y' + 2xy = 2x^3$.

___Solution___:

$$
\begin{aligned}
p(x) &= 2x \\
q(x) &= 2x^3 \\
I(x) &= e^{\int 2x \ dx} = e^{x^2}
\end{aligned}
$$

$$
\begin{aligned}
\left[e^{x^2} \cdot y\right]' &= 2x^3 e^{x^2} \\
\implies e^{x^2} \cdot y &= \int 2x^3 e^{x^2} dx \\
&= x^2 e^{x^2} - e^{x^2} + C \\
\implies y &= \frac{x^2 e^{x^2} - e^{x^2} + C}{e^{x^2}}
\end{aligned}
$$

---

___Example___: Solve $y' = \sin(x) \ (y \sec(x) - 2), \ \ 0 < x < \frac\pi2$

___Solution___:

$$
\begin{aligned}
y' &= \sin(x) \ (y \sec(x) - 2), \ \ 0 < x < \frac\pi2 \\
&= (\tan(x)) y - 2 \sin x \\
y' &- (\tan(x)) y = -2 \sin x \\[1em]
p(x) &= - \tan x \\
q(x) &= -2 \sin x \\
I(x) &= e^{- \int \tan x \ dx} = e^{- \ln |\sec x|} = |\cos x| = \cos x
\end{aligned}
$$

($\cos x$ will always be positive because of our $x$ bounds.)

$$
\begin{aligned}
\left[ \cos x \cdot y \right]' &= -2 \sin x \cos x \\
\implies \cos x \cdot y &= -2 \int \sin x \cos x \ dx \\
&= -2 \cdot \frac{\sin^2 x}{2} + C \\
&= -\sin^2 x + C \\
y &= \frac{-\sin^2 x + C}{\cos x}
\end{aligned}
$$

---

# Mixing Problems, 2

$$
\begin{aligned}
&\frac{dA}{dt} + \frac{r_2}{V} \cdot A = r_1 C_1 \\[1em]
&V(t) = V_0 + (r_1 - r_2) \ t
\end{aligned}
$$

___Example___: A tank initially has 600 liters of a solution with 1500 grams of salt in it.
A solution with 5 g/L flows into the tank at 6 L/min. The mixture leaks out at 3 L/min.
How much salt is in the tank after 1 hour?

___Solution___:

$$
\begin{aligned}
r_1 &= 6& C_1 &= 5 \\
r_2 &= 3& V &= 600 + 3t \\
A(0) &= 1500
\end{aligned}
$$

$$
\begin{aligned}
\frac{dA}{dt} + \frac{3}{600 + 3t} A &= 30 \\
\frac{dA}{dt} + \frac{1}{200 + t}A &= 30 \\[1em]

p(t) &= \frac{1}{200 + t} \\
q(t) &= 30 \\
I(t) &= e^{\int p(t) \ dt} = e^{\frac{1}{200 + t} \ dt} = 200 + t
\end{aligned}
$$

($t$ is already always positive.)

$$
\begin{aligned}
\implies \left[(200 + t) \cdot A\right]' &= 300 (200 + t) \\
&= 6000 + 30t \\
\implies (200 + t) \cdot A &= \int 6000 + 30t \ dt \\
&= 6000t + 15t^2 + C \\
A &= \frac{6000t + 15t^2 + C}{200 + t}
\end{aligned}
$$

$$
\begin{aligned}
A(0) &= 1500 \\
\implies 1500 &= \frac{C}{200} \implies C + 300000
\end{aligned}
$$

$$
\begin{aligned}
A(t) &= \frac{6000t + 15t^2 + 300000}{200 + t} \\
A(60) &= \frac{6000(60) + 15(60)^2 + 300000}{260}
\end{aligned}
$$

---

# Practice: Separable and Linear DEs

___Example___: Solve $(x+1) \frac{dy}{dx} = x + 6$.

___Solution___:

This DE is separable.

$$
\begin{aligned}
\int dy &= \int \frac{x+6}{x+1} \ dx \\
&= \int \frac{u+5}{u} \ du \\
&= \int 1 + \frac5u \ du \\
&= u + 5 \ln |u| + C \\
y &= (x+1) + 5 \ln |x+1| + C \\
\end{aligned}
$$

---

___Example___: Solve $x \frac{dy}{dx} - 4y = x^6 e^x$.


___Solution___:

This DE is linear.

$$
\begin{aligned}
y' - \left(\frac{4}{x}\right) y = x^5 e^x \\
\end{aligned}
$$

$$
\begin{aligned}
p(x) &= - \left(\frac{4}{x}\right)y \\
g(x) &= x^5 e^x \\[.5em]
I(x) &= e^{\int p(x) \ dx} \\
&= e^{-\int \frac4x \ dx} \\
&= e^{-4 \ln |x|} \\
&= |x|^{-4} \\
&= \frac1{x^4}
\end{aligned}
$$

$$
\begin{aligned}
\left[\frac1{x^4} \cdot y\right]' &= \frac{x^5 e^x}{x^4} = xe^x \\
\implies \frac1{x^4} \cdot y &= \int xe^x \ dx \\
&= xe^x - e^x + C \\[.5em]
y &= x^4 \left(xe^x - e^x + C\right)
\end{aligned}
$$

---

___Example___: Solve $\frac{dy}{dx} + 2xy = x$.

___Solution___:

This DE is both separable and linear.

Separable solution:

$$
\begin{aligned}
\dydx &= x - 2xy \\
&= x(1 - 2y) \\
\frac1{1 - 2y} \cdot \dydx &= x \\
\frac1{1-2y} \ dy&= x \ dx \\
\implies \int \frac1{1-2y} \ dy &= \int x \ dx \\
-\frac{\ln |1-2y|}2 &= \frac12 x^2 \\
- \ln |1-2y| &= x^2 \\
- |1-2y| &= e^{x^2} \\
1-2y &= \plusmn e^{x^2} \\
y &= \frac{1 \plusmn e^{x^2}}2
\end{aligned}
$$